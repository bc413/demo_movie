import { LocalService } from "../../service/locailService";
import { INFOR_USER } from "../constant/constant";

const initialState = {
  dataMovie: LocalService.get(),
};

let movieReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case INFOR_USER: {
      return { ...state, dataMovie: payload };
    }
    default:
      return state;
  }
};
export default movieReducer;
