import { LocalService } from "../../service/locailService";
import { axiosMovie } from "../../service/useService";
import { INFOR_USER } from "../constant/constant";

export const setActionMovie = (values) => {
  return {
    type: INFOR_USER,
    payload: values,
  };
};

export let setActionMovieThunk = (values, onSuccess) => {
  return (dispatch) => {
    axiosMovie
      .handleLogin(values)
      .then((res) => {
        console.log(res);
        dispatch({
          type: INFOR_USER,
          payload: res.data.content,
        });
        LocalService.set(res.data.content);
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
