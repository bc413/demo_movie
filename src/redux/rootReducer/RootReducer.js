import { combineReducers } from "redux";
import movieReducer from "../movieReducer/MovieReducer";

export let rootReducerMovie = combineReducers({
  movieReducer,
});
