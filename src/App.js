import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./page/HomePage/HomePage";
import LoginPage from "./page/LoginPage/LoginPage";
import DetailPage from "./page/DetailPage/DetailPage";
import NotFoundPage from "./page/NotFoundPage/NotFoundPage";
import Layout from "./Layout/Layout";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout Components={HomePage} />} />
        <Route path="/login" element={<LoginPage />} />
        <Route
          path="/detail/:id"
          element={<Layout Components={DetailPage} />}
        />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
