import { Button, Checkbox, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { INFOR_USER } from "../../redux/constant/constant";
import { LocalService } from "../../service/locailService";
import { axiosMovie } from "../../service/useService";
import Lottie from "lottie-react";
import bg_lottie from "../../asset/animateLogin.json";
import {
  setActionMovie,
  setActionMovieThunk,
} from "../../redux/actionMovie/actionMovie";

const LoginPage = () => {
  let navLink = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    axiosMovie
      .handleLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công!");
        console.log(res);
        dispatch(setActionMovie(res.data.content));
        LocalService.set(res.data.content);
        navLink("/");
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại!");
        console.log(err);
      });
  };
  const onFinishThunk = (values) => {
    let handleSuccess = () => {
      message.success("Đăng nhập thành công!");
      navLink("/");
    };
    dispatch(setActionMovieThunk(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="h-screen bg-orange-500 flex flex-col items-center justify-center">
      <div className="flex">
        <div className="w-1/2 h-full">
          <Lottie animationData={bg_lottie} loop={true} />
        </div>
        <div className="p-12 w-1/2 h-full bg-white border-4	rounded">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Checkbox>Remember me</Checkbox>
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button className="bg-blue-300" type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};
export default LoginPage;
