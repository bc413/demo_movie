import React from "react";

import ListMovie from "../../components/ListMovie";
import TabsMovie from "./TabsMovie/TabsMovie";

export default function HomePage() {
  return (
    <div className="space-y-10">
      <div>
        <ListMovie />
      </div>
      <div>
        <TabsMovie />
      </div>
    </div>
  );
}
