import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { layDanhSachPhim } from "../../../service/movieSerive";
import ItemTabsMovie from "./ItemTabsMovie";

export default function TabsMovie() {
  const onChange = (key) => {
    console.log(key);
  };
  const items = [
    {
      key: "1",
      label: `Tab 1`,
      children: `Content of Tab Pane 1`,
    },
  ];
  const [lichChieuPhim, setLichChieuPhim] = useState([]);
  useEffect(() => {
    layDanhSachPhim
      .layThongTinLichChieu()
      .then((res) => {
        setLichChieuPhim(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log(lichChieuPhim);

  let renderRapChieuPhim = () => {
    return lichChieuPhim.map((item, index) => {
      return {
        key: item.maHeThongRap,
        label: <img style={{ width: "80px" }} src={item.logo} alt="" />,
        children: (
          <Tabs
            style={{ height: "500px" }}
            defaultActiveKey="1"
            items={item.lstCumRap.map((item, index) => {
              return {
                key: index,
                label: (
                  <div className="text-left">
                    <p>{item.tenCumRap}</p>
                    <p className="text-red-600">[Chi Tiết]</p>
                  </div>
                ),
                children: (
                  <div style={{ height: 500, overflowY: "scroll" }}>
                    <div>
                      {item.danhSachPhim.map((item) => {
                        return (
                          <div className="flex">
                            <div>
                              <img
                                style={{ width: "80px" }}
                                src={item.hinhAnh}
                                alt=""
                              />
                            </div>
                            <div className="row">
                              {item.lstLichChieuTheoPhim
                                .slice(0, 4)
                                .map((item) => {
                                  return <ItemTabsMovie item={item} />;
                                })}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                ),
              };
            })}
            onChange={onChange}
            tabPosition={"left"}
          />
        ),
      };
    });
  };
  return (
    <div className="container">
      <Tabs
        style={{ height: "500px" }}
        defaultActiveKey="1"
        items={renderRapChieuPhim()}
        onChange={onChange}
        tabPosition={"left"}
      />
    </div>
  );
}
