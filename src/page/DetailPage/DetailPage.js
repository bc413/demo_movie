import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { layDanhSachPhim } from "../../service/movieSerive";

export default function DetailPage() {
  let params = useParams();
  const [detaiMovie, setDetaiMovie] = useState({});
  useEffect(() => {
    layDanhSachPhim
      .getDetailMovie(params.id)
      .then((res) => {
        console.log(res);
        setDetaiMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log(detaiMovie);
  return (
    <div className="bg-orange-400 h-screen  flex flex-col justify-center">
      <div className="  flex container ">
        <div>
          <img
            src={detaiMovie.hinhAnh}
            style={{ height: "300px", borderRadius: "10px" }}
            alt=""
          />
        </div>
        <div className="ml-2	items-start bg-white rounded-lg p-3 w-96">
          <p>
            <strong>Name:</strong> {detaiMovie.tenPhim}
          </p>
          <p className="my-3">
            <strong>Des:</strong> {detaiMovie.moTa}
          </p>
          <p>
            <strong>Ngày khởi chiếu:</strong> {detaiMovie.ngayKhoiChieu}
          </p>
        </div>
      </div>
      <NavLink to="/">
        <div className="text-white">Close</div>
      </NavLink>
    </div>
  );
}
