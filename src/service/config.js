import axios from "axios";

export const URL_BASE = "https://movienew.cybersoft.edu.vn";
let Token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q";

export let TokenCybersoft = () => {
  return {
    TokenCybersoft: Token,
  };
};

export let https = axios.create({
  baseURL: URL_BASE,
  headers: TokenCybersoft(),
});
