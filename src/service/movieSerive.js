import axios from "axios";
import { https, TokenCybersoft, URL_BASE } from "./config";

export let layDanhSachPhim = {
  getListMovie: () => {
    // return axios({
    //   url: `${URL_BASE}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`,
    //   method: "GET",
    //   headers: TokenCybersoft(),
    // });
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04");
  },
  getDetailMovie: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  },
  layThongTinLichChieu: () => {
    return https.get(
      "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01"
    );
  },
};

// redux Thunk
