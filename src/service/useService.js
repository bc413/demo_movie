import axios from "axios";
import { TokenCybersoft, URL_BASE } from "./config";

export let axiosMovie = {
  handleLogin: (data) => {
    return axios({
      url: `${URL_BASE}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: data,
      headers: TokenCybersoft(),
    });
  },
};
