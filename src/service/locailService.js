export let MY_CART = "myCat";

export let LocalService = {
  get: () => {
    const GetData = localStorage.getItem(MY_CART);
    let PaseData = JSON.parse(GetData);
    return PaseData;
  },
  set: (data) => {
    let convertData = JSON.stringify(data);
    localStorage.setItem(MY_CART, convertData);
  },
  romove: () => {
    localStorage.removeItem(MY_CART);
  },
};
