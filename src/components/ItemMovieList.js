import React from "react";
import { Card, Button } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function ItemMovieList({ item }) {
  return (
    <div>
      <Card
        className="text-center"
        hoverable
        style={{
          width: 240,
        }}
        cover={
          <img
            style={{ height: "250px", objectFit: "cover" }}
            alt="example"
            src={item.hinhAnh}
          />
        }
      >
        <Meta title={item.tenPhim} />

        <NavLink to={`/detail/${item.maPhim}`}>
          <Button className="mt-2 bg-lime-500 text-white">Xem chi tiết</Button>
        </NavLink>
      </Card>
    </div>
  );
}
