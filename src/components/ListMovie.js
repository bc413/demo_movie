import React, { useEffect } from "react";
import { useState } from "react";
import { layDanhSachPhim } from "../service/movieSerive";
import ItemMovieList from "./ItemMovieList";

export default function ListMovie() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    layDanhSachPhim
      .getListMovie()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderMovie = () => {
    return dataMovie.map((item, index) => {
      return <ItemMovieList item={item} key={index} />;
    });
  };
  return (
    <div className="container grid grid-cols-4 gap-4 py-5">{renderMovie()}</div>
  );
}
