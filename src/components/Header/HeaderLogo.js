import React from "react";

export default function HeaderLogo() {
  return (
    <div>
      <img
        src="https://demo1.cybersoft.edu.vn/logo.png"
        style={{ width: "200px" }}
        alt=""
      />
    </div>
  );
}
