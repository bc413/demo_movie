import React from "react";
import HeaderLogo from "./HeaderLogo";
import HeaderMenu from "./HeaderMenu";
export default function Header() {
  return (
    <div className="bg-slate-500">
      <div className="flex items-center justify-between container">
        <HeaderLogo />
        <HeaderMenu />
      </div>
    </div>
  );
}
