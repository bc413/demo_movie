import { Button } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { LocalService } from "../../service/locailService";

export default function HeaderMenu() {
  let inforUser = useSelector((state) => {
    return state.movieReducer.dataMovie;
  });
  let handleRemoveLocal = () => {
    LocalService.romove();
    window.location.reload();
  };
  let renderLogin = () => {
    if (inforUser) {
      return (
        <div className="flex items-center ">
          <p>{inforUser.hoTen}</p>

          <Button onClick={handleRemoveLocal}>Đăng xuất</Button>
        </div>
      );
    } else {
      return (
        <div>
          <NavLink to={"/login"}>
            <Button>Đăng nhập</Button>
          </NavLink>
          <Button>Đăng ký</Button>
        </div>
      );
    }
  };
  return (
    <div>
      <p>{renderLogin()}</p>
    </div>
  );
}
